#include "PolynomialManager.h"
#include "RadialFunctions.h"
#include "FeatureExtraction.h"
#define M_PI 3.1415926535897932
#include <limits>
#include <string>

using namespace fe;
using namespace cv;

class ShiftedChebyshev : public fe::PolynomialManager
{
    virtual std::string GetType()
    {
        return "Shifted Chebyshev Polynom by Ishenko Dmitry";
    }

	virtual void InitBasis(int n_max, int diameter) override
	{
		polynomials.resize(n_max);
		Mat matre = Mat::zeros(diameter, diameter, CV_64FC1);
		Mat matim = Mat::zeros(diameter, diameter, CV_64FC1);
		double* vectRe = matre.ptr<double>();
		double* vectIm = matim.ptr<double>();
		for (int i = 0; i < n_max; i++)
		{
			for (int j = 0; j < n_max; j++)
			{
				for (int k = 0; k < diameter; k++)
				{
					for (int l = 0; l < diameter; l++)
					{
						double x = (l - diameter / 2) / double(diameter / 2);
						double y = -(k - diameter / 2) / double(diameter / 2);
						double radial = rf::RadialFunctions::ShiftedChebyshev(sqrt(y * y + x * x), i);
						double fi = atan2(y, x);
						if (abs(x) < 1e-3 && abs(y) < 1e-3) fi = 0;
						vectRe[diameter * k + l] = radial * cos(j * fi);
						vectIm[diameter * k + l] = radial * sin(j * fi);

					}
				}
				polynomials[i].push_back({ matre.clone(), matim.clone() });

			}
		}
	}

	virtual ComplexMoments Decompose(cv::Mat blob) override
	{
		Mat blob_double = Mat::zeros(blob.rows, blob.cols, CV_64FC1);
		blob.convertTo(blob_double, CV_64FC1, 1.0 / 255 / blob.cols / blob.rows);
		int polynomials_cont = 0;
		for (auto& polynom_line : polynomials)
		{
			polynomials_cont += polynom_line.size();
		}
		ComplexMoments dec;
		dec.abs = Mat::zeros(Size(polynomials_cont, 1), CV_64FC1);
		dec.phase = Mat::zeros(Size(polynomials_cont, 1), CV_64FC1);
		dec.re = Mat::zeros(Size(polynomials_cont, 1), CV_64FC1);
		dec.im = Mat::zeros(Size(polynomials_cont, 1), CV_64FC1);
		//переменные
		double* re = dec.re.ptr<double>();
		double* im = dec.im.ptr<double>();
		double* phase = dec.phase.ptr<double>();
		double* abs = dec.abs.ptr<double>();
		//скалярное произведение 
		size_t idx = 0;
		for (auto& pl : polynomials)
		{
			for (auto& p : pl)
			{
				//минус- сопряжение
				re[idx] = sum(blob_double.mul(p.first))[0];
				im[idx] = -sum(blob_double.mul(p.second))[0];
				phase[idx] = atan2(im[idx], re[idx]);
				abs[idx] = sqrt(re[idx] * re[idx] + im[idx] * im[idx]);
				idx++;
			}
		}
		//нормировка
		double norm = cv::norm(dec.abs);
		dec.abs /= norm;
		dec.re /= norm;
		dec.im /= norm;
		return dec;
	}

	virtual cv::Mat Recovery(ComplexMoments& decomposition) override
	{
		int size = polynomials[0][0].first.cols;
		int n_max = polynomials.size() - 1;
		// восстан картинки по полученному изображению
		Mat recovery = Mat::zeros(size, size, CV_64FC1);
		int idx = 0;
		for (auto& polynomials_line : polynomials)
		{
			for (auto& polynomial : polynomials_line)
			{
				recovery += decomposition.re.ptr<double>()[idx] * polynomial.first - decomposition.im.ptr<double>()[idx] * polynomial.second;
				idx++;
			}
		}
		return recovery;

	}
};

std::shared_ptr<fe::PolynomialManager> fe::CreatePolynomialManager()
{
    return std::make_shared<ShiftedChebyshev>();
}